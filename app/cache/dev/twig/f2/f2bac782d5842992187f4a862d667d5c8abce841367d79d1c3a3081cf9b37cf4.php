<?php

/* base.html.twig */
class __TwigTemplate_cf97aa48b84e0d88a34a445cd6e7aa49c4f4fdf9e2ba3c796d41a41099294dfb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_48920a349eaef2af9c33c9eebecc555cdd226e734cb7ac493abd0fc55fc3b197 = $this->env->getExtension("native_profiler");
        $__internal_48920a349eaef2af9c33c9eebecc555cdd226e734cb7ac493abd0fc55fc3b197->enter($__internal_48920a349eaef2af9c33c9eebecc555cdd226e734cb7ac493abd0fc55fc3b197_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\" />
    <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "    <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
</head>
<body>
";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "</body>
</html>
<style>
    body { background: #F5F5F5; font: 18px/1.5 sans-serif; }
    h1, h2 { line-height: 1.2; margin: 0 0 .5em; }
    h1 { font-size: 36px; }
    h2 { font-size: 21px; margin-bottom: 1em; }
    p { margin: 0 0 1em 0; }
    a { color: #0000F0; }
    a:hover { text-decoration: none; }
    code { background: #F5F5F5; max-width: 100px; padding: 2px 6px; word-wrap: break-word; }
    #wrapper { background: #FFF; margin: 1em auto; max-width: 800px; width: 95%; }
    #container { padding: 2em; }
    #welcome, #status { margin-bottom: 2em; }
    #welcome h1 span { display: block; font-size: 75%; }
    #icon-status, #icon-book { float: left; height: 64px; margin-right: 1em; margin-top: -4px; width: 64px; }
    #icon-book { display: none; }

    @media (min-width: 768px) {
        #wrapper { width: 80%; margin: 2em auto; }
        #icon-book { display: inline-block; }
        #status a, #next a { display: block; }

        @-webkit-keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }
        @keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }
        .sf-toolbar { opacity: 0; -webkit-animation: fade-in 1s .2s forwards; animation: fade-in 1s .2s forwards;}
    }
</style>";
        
        $__internal_48920a349eaef2af9c33c9eebecc555cdd226e734cb7ac493abd0fc55fc3b197->leave($__internal_48920a349eaef2af9c33c9eebecc555cdd226e734cb7ac493abd0fc55fc3b197_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_3c7fd76912fb47d486ea1c0b87d2b4f9236a312d4a4fe87044991ce9dfc30c91 = $this->env->getExtension("native_profiler");
        $__internal_3c7fd76912fb47d486ea1c0b87d2b4f9236a312d4a4fe87044991ce9dfc30c91->enter($__internal_3c7fd76912fb47d486ea1c0b87d2b4f9236a312d4a4fe87044991ce9dfc30c91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_3c7fd76912fb47d486ea1c0b87d2b4f9236a312d4a4fe87044991ce9dfc30c91->leave($__internal_3c7fd76912fb47d486ea1c0b87d2b4f9236a312d4a4fe87044991ce9dfc30c91_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_926bf6e09fe4fe9b90e379e20e73ccb559744f21dd500be9317295c69212d44a = $this->env->getExtension("native_profiler");
        $__internal_926bf6e09fe4fe9b90e379e20e73ccb559744f21dd500be9317295c69212d44a->enter($__internal_926bf6e09fe4fe9b90e379e20e73ccb559744f21dd500be9317295c69212d44a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_926bf6e09fe4fe9b90e379e20e73ccb559744f21dd500be9317295c69212d44a->leave($__internal_926bf6e09fe4fe9b90e379e20e73ccb559744f21dd500be9317295c69212d44a_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_a5bf4e30acb710f131fa8640936978e718d32bfd41b867538f1113cd9a9500af = $this->env->getExtension("native_profiler");
        $__internal_a5bf4e30acb710f131fa8640936978e718d32bfd41b867538f1113cd9a9500af->enter($__internal_a5bf4e30acb710f131fa8640936978e718d32bfd41b867538f1113cd9a9500af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_a5bf4e30acb710f131fa8640936978e718d32bfd41b867538f1113cd9a9500af->leave($__internal_a5bf4e30acb710f131fa8640936978e718d32bfd41b867538f1113cd9a9500af_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_aa0423a65fe4d80001e41d0c95d5bad9698a7ff2fa54e5632be55256cafc8080 = $this->env->getExtension("native_profiler");
        $__internal_aa0423a65fe4d80001e41d0c95d5bad9698a7ff2fa54e5632be55256cafc8080->enter($__internal_aa0423a65fe4d80001e41d0c95d5bad9698a7ff2fa54e5632be55256cafc8080_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_aa0423a65fe4d80001e41d0c95d5bad9698a7ff2fa54e5632be55256cafc8080->leave($__internal_aa0423a65fe4d80001e41d0c95d5bad9698a7ff2fa54e5632be55256cafc8080_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 11,  106 => 10,  95 => 6,  83 => 5,  49 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/* <head>*/
/*     <meta charset="UTF-8" />*/
/*     <title>{% block title %}Welcome!{% endblock %}</title>*/
/*     {% block stylesheets %}{% endblock %}*/
/*     <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/* </head>*/
/* <body>*/
/* {% block body %}{% endblock %}*/
/* {% block javascripts %}{% endblock %}*/
/* </body>*/
/* </html>*/
/* <style>*/
/*     body { background: #F5F5F5; font: 18px/1.5 sans-serif; }*/
/*     h1, h2 { line-height: 1.2; margin: 0 0 .5em; }*/
/*     h1 { font-size: 36px; }*/
/*     h2 { font-size: 21px; margin-bottom: 1em; }*/
/*     p { margin: 0 0 1em 0; }*/
/*     a { color: #0000F0; }*/
/*     a:hover { text-decoration: none; }*/
/*     code { background: #F5F5F5; max-width: 100px; padding: 2px 6px; word-wrap: break-word; }*/
/*     #wrapper { background: #FFF; margin: 1em auto; max-width: 800px; width: 95%; }*/
/*     #container { padding: 2em; }*/
/*     #welcome, #status { margin-bottom: 2em; }*/
/*     #welcome h1 span { display: block; font-size: 75%; }*/
/*     #icon-status, #icon-book { float: left; height: 64px; margin-right: 1em; margin-top: -4px; width: 64px; }*/
/*     #icon-book { display: none; }*/
/* */
/*     @media (min-width: 768px) {*/
/*         #wrapper { width: 80%; margin: 2em auto; }*/
/*         #icon-book { display: inline-block; }*/
/*         #status a, #next a { display: block; }*/
/* */
/*         @-webkit-keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }*/
/*         @keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }*/
/*         .sf-toolbar { opacity: 0; -webkit-animation: fade-in 1s .2s forwards; animation: fade-in 1s .2s forwards;}*/
/*     }*/
/* </style>*/
