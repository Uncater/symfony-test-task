<?php

/* FOSUserBundle::layout.html.twig */
class __TwigTemplate_d2ff726eb369bd12d92e6291defa335e9eef0c3b2e4099afde9608ea3a4830e1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "FOSUserBundle::layout.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a232cb57cf84b224a4d71c8078a3714b01f8ade59e0f4d23263e726b940f066b = $this->env->getExtension("native_profiler");
        $__internal_a232cb57cf84b224a4d71c8078a3714b01f8ade59e0f4d23263e726b940f066b->enter($__internal_a232cb57cf84b224a4d71c8078a3714b01f8ade59e0f4d23263e726b940f066b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle::layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a232cb57cf84b224a4d71c8078a3714b01f8ade59e0f4d23263e726b940f066b->leave($__internal_a232cb57cf84b224a4d71c8078a3714b01f8ade59e0f4d23263e726b940f066b_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_a500835ab4f062a2f48082671de096e5240fb6d081aefb194c89e650d51d2cf3 = $this->env->getExtension("native_profiler");
        $__internal_a500835ab4f062a2f48082671de096e5240fb6d081aefb194c89e650d51d2cf3->enter($__internal_a500835ab4f062a2f48082671de096e5240fb6d081aefb194c89e650d51d2cf3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Referral links applacation";
        
        $__internal_a500835ab4f062a2f48082671de096e5240fb6d081aefb194c89e650d51d2cf3->leave($__internal_a500835ab4f062a2f48082671de096e5240fb6d081aefb194c89e650d51d2cf3_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_ea6068a689b0cba4271ae56c89e210ad3988357e9a5d57d731f3d701f694baa8 = $this->env->getExtension("native_profiler");
        $__internal_ea6068a689b0cba4271ae56c89e210ad3988357e9a5d57d731f3d701f694baa8->enter($__internal_ea6068a689b0cba4271ae56c89e210ad3988357e9a5d57d731f3d701f694baa8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div id=\"wrapper\">
    <div id=\"container\">
    <div id=\"welcome\">
        <h1><span>Welcome!</h1>
    </div>
    <div class=\"block\">
    ";
        // line 12
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 13
        echo "</div>
    </div>
    </div>

";
        
        $__internal_ea6068a689b0cba4271ae56c89e210ad3988357e9a5d57d731f3d701f694baa8->leave($__internal_ea6068a689b0cba4271ae56c89e210ad3988357e9a5d57d731f3d701f694baa8_prof);

    }

    // line 12
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_c6c53df8558053ec0c310ec90d0d42f3b1d9feccd164e53e6292b86977d21a63 = $this->env->getExtension("native_profiler");
        $__internal_c6c53df8558053ec0c310ec90d0d42f3b1d9feccd164e53e6292b86977d21a63->enter($__internal_c6c53df8558053ec0c310ec90d0d42f3b1d9feccd164e53e6292b86977d21a63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        
        $__internal_c6c53df8558053ec0c310ec90d0d42f3b1d9feccd164e53e6292b86977d21a63->leave($__internal_c6c53df8558053ec0c310ec90d0d42f3b1d9feccd164e53e6292b86977d21a63_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 12,  64 => 13,  62 => 12,  54 => 6,  48 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block title %}Referral links applacation{% endblock %}*/
/* */
/* {% block body %}*/
/*     <div id="wrapper">*/
/*     <div id="container">*/
/*     <div id="welcome">*/
/*         <h1><span>Welcome!</h1>*/
/*     </div>*/
/*     <div class="block">*/
/*     {% block fos_user_content %}{% endblock %}*/
/* </div>*/
/*     </div>*/
/*     </div>*/
/* */
/* {% endblock %}*/
