<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_64b78e263718d72f69d049cf123faeb925a531a6d23c2eca14bb8f3e65e0dbdb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_db4651b2d53a30cd4cbc5988eb6578e1d580e53b1885c0fd8cf55ded670c148c = $this->env->getExtension("native_profiler");
        $__internal_db4651b2d53a30cd4cbc5988eb6578e1d580e53b1885c0fd8cf55ded670c148c->enter($__internal_db4651b2d53a30cd4cbc5988eb6578e1d580e53b1885c0fd8cf55ded670c148c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_db4651b2d53a30cd4cbc5988eb6578e1d580e53b1885c0fd8cf55ded670c148c->leave($__internal_db4651b2d53a30cd4cbc5988eb6578e1d580e53b1885c0fd8cf55ded670c148c_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
