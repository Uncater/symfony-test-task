<?php

/* FOSUserBundle:Group:new.html.twig */
class __TwigTemplate_2c18dc507280fbba28062d9436ebe04fe2f55a6483e05cd80eabb387094765ac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Group:new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_91cb2a4d3f07c2be9a69eeef7b3b3d743f9869c802af537ea050c652501b445b = $this->env->getExtension("native_profiler");
        $__internal_91cb2a4d3f07c2be9a69eeef7b3b3d743f9869c802af537ea050c652501b445b->enter($__internal_91cb2a4d3f07c2be9a69eeef7b3b3d743f9869c802af537ea050c652501b445b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_91cb2a4d3f07c2be9a69eeef7b3b3d743f9869c802af537ea050c652501b445b->leave($__internal_91cb2a4d3f07c2be9a69eeef7b3b3d743f9869c802af537ea050c652501b445b_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_a8afb33f7f37d84d7a89a22f82573d52446e38281d79843fe6e18e4856772c04 = $this->env->getExtension("native_profiler");
        $__internal_a8afb33f7f37d84d7a89a22f82573d52446e38281d79843fe6e18e4856772c04->enter($__internal_a8afb33f7f37d84d7a89a22f82573d52446e38281d79843fe6e18e4856772c04_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:new_content.html.twig", "FOSUserBundle:Group:new.html.twig", 4)->display($context);
        
        $__internal_a8afb33f7f37d84d7a89a22f82573d52446e38281d79843fe6e18e4856772c04->leave($__internal_a8afb33f7f37d84d7a89a22f82573d52446e38281d79843fe6e18e4856772c04_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:new_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
