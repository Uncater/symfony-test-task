<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_8cab6d6c945fb9b8756622c28434ecc408d0fd0d3f78d1a935a6501c0c35a047 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f64aba7d47e8648bf45e275f4c5ceb86282db4bb975b3943ea36084739ee0ab6 = $this->env->getExtension("native_profiler");
        $__internal_f64aba7d47e8648bf45e275f4c5ceb86282db4bb975b3943ea36084739ee0ab6->enter($__internal_f64aba7d47e8648bf45e275f4c5ceb86282db4bb975b3943ea36084739ee0ab6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_f64aba7d47e8648bf45e275f4c5ceb86282db4bb975b3943ea36084739ee0ab6->leave($__internal_f64aba7d47e8648bf45e275f4c5ceb86282db4bb975b3943ea36084739ee0ab6_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}*/
/* */
