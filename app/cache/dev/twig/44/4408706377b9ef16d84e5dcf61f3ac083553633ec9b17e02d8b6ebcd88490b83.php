<?php

/* ::form.html.twig */
class __TwigTemplate_f75716f052e7364e841c489ab6d2746dc4f299205dcf6babc49d55e4414abcbf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b63e18b93991fe4eebc8259f6d0c60e54e24e8a0d0eda08ee5d27ef0b22f4d85 = $this->env->getExtension("native_profiler");
        $__internal_b63e18b93991fe4eebc8259f6d0c60e54e24e8a0d0eda08ee5d27ef0b22f4d85->enter($__internal_b63e18b93991fe4eebc8259f6d0c60e54e24e8a0d0eda08ee5d27ef0b22f4d85_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::form.html.twig"));

        // line 1
        echo "<div class=\"center\">
    ";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : $this->getContext($context, "message")), "html", null, true);
        echo "<br>
    <button type=\"button\" class=\"\"><a href=\"/app_dev.php\">Go to homepage</a></button></button>
    </div>
<style>
.center {
    margin: auto;
    width: 45%;
    border: 3px solid #73AD21;
    padding: 15px;
}

</style>";
        
        $__internal_b63e18b93991fe4eebc8259f6d0c60e54e24e8a0d0eda08ee5d27ef0b22f4d85->leave($__internal_b63e18b93991fe4eebc8259f6d0c60e54e24e8a0d0eda08ee5d27ef0b22f4d85_prof);

    }

    public function getTemplateName()
    {
        return "::form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* <div class="center">*/
/*     {{ message}}<br>*/
/*     <button type="button" class=""><a href="/app_dev.php">Go to homepage</a></button></button>*/
/*     </div>*/
/* <style>*/
/* .center {*/
/*     margin: auto;*/
/*     width: 45%;*/
/*     border: 3px solid #73AD21;*/
/*     padding: 15px;*/
/* }*/
/* */
/* </style>*/
