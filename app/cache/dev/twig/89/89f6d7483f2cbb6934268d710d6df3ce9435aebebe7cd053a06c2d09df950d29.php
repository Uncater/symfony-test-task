<?php

/* FOSUserBundle:Profile:edit.html.twig */
class __TwigTemplate_2b676a77f6b23227a63e0ab1a9f662b7b3cde4760e19f5b966e930833e87267e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Profile:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_05a4c50f5a3d195546021c508385f5283a9ced7f97975a630524d9017e559443 = $this->env->getExtension("native_profiler");
        $__internal_05a4c50f5a3d195546021c508385f5283a9ced7f97975a630524d9017e559443->enter($__internal_05a4c50f5a3d195546021c508385f5283a9ced7f97975a630524d9017e559443_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_05a4c50f5a3d195546021c508385f5283a9ced7f97975a630524d9017e559443->leave($__internal_05a4c50f5a3d195546021c508385f5283a9ced7f97975a630524d9017e559443_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_fe6ca5b294a661190f6f7052689c05f2e5dadf00e59333163f29b4281a2c1c91 = $this->env->getExtension("native_profiler");
        $__internal_fe6ca5b294a661190f6f7052689c05f2e5dadf00e59333163f29b4281a2c1c91->enter($__internal_fe6ca5b294a661190f6f7052689c05f2e5dadf00e59333163f29b4281a2c1c91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Profile:edit_content.html.twig", "FOSUserBundle:Profile:edit.html.twig", 4)->display($context);
        
        $__internal_fe6ca5b294a661190f6f7052689c05f2e5dadf00e59333163f29b4281a2c1c91->leave($__internal_fe6ca5b294a661190f6f7052689c05f2e5dadf00e59333163f29b4281a2c1c91_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Profile:edit_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
