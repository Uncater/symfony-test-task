<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_67275f4b14620b7e5d55231603b20d63637889776286e42cac4ae43434346051 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3d6243b7200f62ceb8343ddf5b1a0971804f7aaa53d153d5d3745a271a4fa460 = $this->env->getExtension("native_profiler");
        $__internal_3d6243b7200f62ceb8343ddf5b1a0971804f7aaa53d153d5d3745a271a4fa460->enter($__internal_3d6243b7200f62ceb8343ddf5b1a0971804f7aaa53d153d5d3745a271a4fa460_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("TwigBundle:Exception:exception.txt.twig", "TwigBundle:Exception:exception.css.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_3d6243b7200f62ceb8343ddf5b1a0971804f7aaa53d153d5d3745a271a4fa460->leave($__internal_3d6243b7200f62ceb8343ddf5b1a0971804f7aaa53d153d5d3745a271a4fa460_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include 'TwigBundle:Exception:exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
