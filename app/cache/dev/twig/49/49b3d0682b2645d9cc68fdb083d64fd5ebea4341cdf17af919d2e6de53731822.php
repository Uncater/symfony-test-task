<?php

/* FOSUserBundle:Resetting:checkEmail.html.twig */
class __TwigTemplate_7addf362eae776fc38cd3eb98a34e20d0c0f479b1b16cab37b3bbcb3c86a1f05 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Resetting:checkEmail.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2f49d14f8900a2a864d885f97bcace2b8ef95171fe7a9d7e1c3f4f7935e1f78b = $this->env->getExtension("native_profiler");
        $__internal_2f49d14f8900a2a864d885f97bcace2b8ef95171fe7a9d7e1c3f4f7935e1f78b->enter($__internal_2f49d14f8900a2a864d885f97bcace2b8ef95171fe7a9d7e1c3f4f7935e1f78b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:checkEmail.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2f49d14f8900a2a864d885f97bcace2b8ef95171fe7a9d7e1c3f4f7935e1f78b->leave($__internal_2f49d14f8900a2a864d885f97bcace2b8ef95171fe7a9d7e1c3f4f7935e1f78b_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_db039f7924986aa3d77d87f5620be00302386769de39121ce4d798c7edec5c0a = $this->env->getExtension("native_profiler");
        $__internal_db039f7924986aa3d77d87f5620be00302386769de39121ce4d798c7edec5c0a->enter($__internal_db039f7924986aa3d77d87f5620be00302386769de39121ce4d798c7edec5c0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.check_email", array("%email%" => (isset($context["email"]) ? $context["email"] : $this->getContext($context, "email"))), "FOSUserBundle"), "html", null, true);
        echo "
</p>
";
        
        $__internal_db039f7924986aa3d77d87f5620be00302386769de39121ce4d798c7edec5c0a->leave($__internal_db039f7924986aa3d77d87f5620be00302386769de39121ce4d798c7edec5c0a_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:checkEmail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 7,  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/* <p>*/
/* {{ 'resetting.check_email'|trans({'%email%': email}) }}*/
/* </p>*/
/* {% endblock %}*/
/* */
