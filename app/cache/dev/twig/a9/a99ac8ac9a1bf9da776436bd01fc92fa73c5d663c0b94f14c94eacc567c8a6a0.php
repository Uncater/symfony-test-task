<?php

/* FOSUserBundle:Group:edit.html.twig */
class __TwigTemplate_6e830064ed79fd0f11f82f635958eb571ce2572593e4bf267a293430e5b76e28 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Group:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b8b03ffce991fdefa073f14bc713af8e61e7fdc7c3cb24701cde1e1a0ca60872 = $this->env->getExtension("native_profiler");
        $__internal_b8b03ffce991fdefa073f14bc713af8e61e7fdc7c3cb24701cde1e1a0ca60872->enter($__internal_b8b03ffce991fdefa073f14bc713af8e61e7fdc7c3cb24701cde1e1a0ca60872_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b8b03ffce991fdefa073f14bc713af8e61e7fdc7c3cb24701cde1e1a0ca60872->leave($__internal_b8b03ffce991fdefa073f14bc713af8e61e7fdc7c3cb24701cde1e1a0ca60872_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_b6a430b7768cad39985b10913833e4e23d347c7e80e253dcce91e5200d8465d7 = $this->env->getExtension("native_profiler");
        $__internal_b6a430b7768cad39985b10913833e4e23d347c7e80e253dcce91e5200d8465d7->enter($__internal_b6a430b7768cad39985b10913833e4e23d347c7e80e253dcce91e5200d8465d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:edit_content.html.twig", "FOSUserBundle:Group:edit.html.twig", 4)->display($context);
        
        $__internal_b6a430b7768cad39985b10913833e4e23d347c7e80e253dcce91e5200d8465d7->leave($__internal_b6a430b7768cad39985b10913833e4e23d347c7e80e253dcce91e5200d8465d7_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:edit_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
