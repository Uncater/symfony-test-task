<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_e1bbec1ddf56b559cd01fe3f178aef4d71f05cb752d1de3c716d6c943a09dd9a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_623129d9a984cb68a57fa002563fc72effdb9b25b87efe68443f726a6a74c630 = $this->env->getExtension("native_profiler");
        $__internal_623129d9a984cb68a57fa002563fc72effdb9b25b87efe68443f726a6a74c630->enter($__internal_623129d9a984cb68a57fa002563fc72effdb9b25b87efe68443f726a6a74c630_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        $this->loadTemplate("TwigBundle:Exception:exception.xml.twig", "TwigBundle:Exception:exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_623129d9a984cb68a57fa002563fc72effdb9b25b87efe68443f726a6a74c630->leave($__internal_623129d9a984cb68a57fa002563fc72effdb9b25b87efe68443f726a6a74c630_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include 'TwigBundle:Exception:exception.xml.twig' with { 'exception': exception } %}*/
/* */
