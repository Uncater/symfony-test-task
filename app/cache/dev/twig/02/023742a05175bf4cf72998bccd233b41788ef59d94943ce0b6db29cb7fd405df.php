<?php

/* FOSUserBundle:Registration:checkEmail.html.twig */
class __TwigTemplate_713126d9e5b147314e06f62707329b1d8a8210eb751c79e0ca57fcf8af857b86 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Registration:checkEmail.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0a1db6bd9d36654d899f6208d073f9afefeef9fa77d12f8620b5e1ad0b331597 = $this->env->getExtension("native_profiler");
        $__internal_0a1db6bd9d36654d899f6208d073f9afefeef9fa77d12f8620b5e1ad0b331597->enter($__internal_0a1db6bd9d36654d899f6208d073f9afefeef9fa77d12f8620b5e1ad0b331597_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:checkEmail.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0a1db6bd9d36654d899f6208d073f9afefeef9fa77d12f8620b5e1ad0b331597->leave($__internal_0a1db6bd9d36654d899f6208d073f9afefeef9fa77d12f8620b5e1ad0b331597_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_f622e76da83b0dd968afa0869c50d75fd0f937b4a4f9a515af9d899567c3020c = $this->env->getExtension("native_profiler");
        $__internal_f622e76da83b0dd968afa0869c50d75fd0f937b4a4f9a515af9d899567c3020c->enter($__internal_f622e76da83b0dd968afa0869c50d75fd0f937b4a4f9a515af9d899567c3020c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.check_email", array("%email%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_f622e76da83b0dd968afa0869c50d75fd0f937b4a4f9a515af9d899567c3020c->leave($__internal_f622e76da83b0dd968afa0869c50d75fd0f937b4a4f9a515af9d899567c3020c_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:checkEmail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/*     <p>{{ 'registration.check_email'|trans({'%email%': user.email}) }}</p>*/
/* {% endblock fos_user_content %}*/
/* */
