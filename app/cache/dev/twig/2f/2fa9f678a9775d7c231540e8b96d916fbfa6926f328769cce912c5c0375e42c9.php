<?php

/* FOSUserBundle:Resetting:email.txt.twig */
class __TwigTemplate_953d412bbcd18b9e17dcf4f07ddd26a8bf76b46a975c1252979615c44c92b093 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cc7fcc4b98fa16d64169ccf1255927754a822ee57d7f5963a1a918c83d38fa0c = $this->env->getExtension("native_profiler");
        $__internal_cc7fcc4b98fa16d64169ccf1255927754a822ee57d7f5963a1a918c83d38fa0c->enter($__internal_cc7fcc4b98fa16d64169ccf1255927754a822ee57d7f5963a1a918c83d38fa0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        $this->displayBlock('body_text', $context, $blocks);
        // line 12
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_cc7fcc4b98fa16d64169ccf1255927754a822ee57d7f5963a1a918c83d38fa0c->leave($__internal_cc7fcc4b98fa16d64169ccf1255927754a822ee57d7f5963a1a918c83d38fa0c_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_3d3030c084fd170ef59c2af59d994856c904ee7e6d5173cdc4694b4592820dda = $this->env->getExtension("native_profiler");
        $__internal_3d3030c084fd170ef59c2af59d994856c904ee7e6d5173cdc4694b4592820dda->enter($__internal_3d3030c084fd170ef59c2af59d994856c904ee7e6d5173cdc4694b4592820dda_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('translator')->trans("resetting.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array())), "FOSUserBundle");
        echo "
";
        
        $__internal_3d3030c084fd170ef59c2af59d994856c904ee7e6d5173cdc4694b4592820dda->leave($__internal_3d3030c084fd170ef59c2af59d994856c904ee7e6d5173cdc4694b4592820dda_prof);

    }

    // line 7
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_e4bf5042e41fde20ea429b971b8c5eb0b0b9eec788d6a31b9fb42cfa4fe8355d = $this->env->getExtension("native_profiler");
        $__internal_e4bf5042e41fde20ea429b971b8c5eb0b0b9eec788d6a31b9fb42cfa4fe8355d->enter($__internal_e4bf5042e41fde20ea429b971b8c5eb0b0b9eec788d6a31b9fb42cfa4fe8355d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 9
        echo $this->env->getExtension('translator')->trans("resetting.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_e4bf5042e41fde20ea429b971b8c5eb0b0b9eec788d6a31b9fb42cfa4fe8355d->leave($__internal_e4bf5042e41fde20ea429b971b8c5eb0b0b9eec788d6a31b9fb42cfa4fe8355d_prof);

    }

    // line 12
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_8c53a6ced757ca8ef5c3542e723db59da0d998e4dc5923ffec83285c39e38482 = $this->env->getExtension("native_profiler");
        $__internal_8c53a6ced757ca8ef5c3542e723db59da0d998e4dc5923ffec83285c39e38482->enter($__internal_8c53a6ced757ca8ef5c3542e723db59da0d998e4dc5923ffec83285c39e38482_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_8c53a6ced757ca8ef5c3542e723db59da0d998e4dc5923ffec83285c39e38482->leave($__internal_8c53a6ced757ca8ef5c3542e723db59da0d998e4dc5923ffec83285c39e38482_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  66 => 12,  57 => 9,  51 => 7,  42 => 4,  36 => 2,  29 => 12,  27 => 7,  25 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* {% block subject %}*/
/* {% autoescape false %}*/
/* {{ 'resetting.email.subject'|trans({'%username%': user.username}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_text %}*/
/* {% autoescape false %}*/
/* {{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_html %}{% endblock %}*/
/* */
