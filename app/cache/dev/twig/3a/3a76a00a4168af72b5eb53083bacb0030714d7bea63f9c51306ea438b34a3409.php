<?php

/* FOSUserBundle:Profile:show.html.twig */
class __TwigTemplate_506d90021d3f0a413612195efec2e795506eb1aa18adbef4d233684c8ac5d606 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Profile:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e86422e8701459d5dd608c995f6f0b481bc2eb6c09e359e5390dfc5a067f9d7e = $this->env->getExtension("native_profiler");
        $__internal_e86422e8701459d5dd608c995f6f0b481bc2eb6c09e359e5390dfc5a067f9d7e->enter($__internal_e86422e8701459d5dd608c995f6f0b481bc2eb6c09e359e5390dfc5a067f9d7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e86422e8701459d5dd608c995f6f0b481bc2eb6c09e359e5390dfc5a067f9d7e->leave($__internal_e86422e8701459d5dd608c995f6f0b481bc2eb6c09e359e5390dfc5a067f9d7e_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_6062a389b776cbb4a41e02e349937b3f22ddf30c0a05833a34ab749939a9747c = $this->env->getExtension("native_profiler");
        $__internal_6062a389b776cbb4a41e02e349937b3f22ddf30c0a05833a34ab749939a9747c->enter($__internal_6062a389b776cbb4a41e02e349937b3f22ddf30c0a05833a34ab749939a9747c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Profile:show_content.html.twig", "FOSUserBundle:Profile:show.html.twig", 4)->display($context);
        
        $__internal_6062a389b776cbb4a41e02e349937b3f22ddf30c0a05833a34ab749939a9747c->leave($__internal_6062a389b776cbb4a41e02e349937b3f22ddf30c0a05833a34ab749939a9747c_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Profile:show_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
