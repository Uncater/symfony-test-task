<?php

/* FOSUserBundle:Group:show.html.twig */
class __TwigTemplate_2a38b285665daf78dfda1be3ef4dc573fd78849846592e9de0a4cf2032de8263 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Group:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_751ef16ac1a57bee9467a9fe0f865e9d6c2f6108f588b463bff23f9859febd62 = $this->env->getExtension("native_profiler");
        $__internal_751ef16ac1a57bee9467a9fe0f865e9d6c2f6108f588b463bff23f9859febd62->enter($__internal_751ef16ac1a57bee9467a9fe0f865e9d6c2f6108f588b463bff23f9859febd62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_751ef16ac1a57bee9467a9fe0f865e9d6c2f6108f588b463bff23f9859febd62->leave($__internal_751ef16ac1a57bee9467a9fe0f865e9d6c2f6108f588b463bff23f9859febd62_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_84de6adf0530a970ff56e1712701e08fbcbe34865c5a496b2196f581620c79cc = $this->env->getExtension("native_profiler");
        $__internal_84de6adf0530a970ff56e1712701e08fbcbe34865c5a496b2196f581620c79cc->enter($__internal_84de6adf0530a970ff56e1712701e08fbcbe34865c5a496b2196f581620c79cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:show_content.html.twig", "FOSUserBundle:Group:show.html.twig", 4)->display($context);
        
        $__internal_84de6adf0530a970ff56e1712701e08fbcbe34865c5a496b2196f581620c79cc->leave($__internal_84de6adf0530a970ff56e1712701e08fbcbe34865c5a496b2196f581620c79cc_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:show_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
