<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_3115e38bc597d3160dac0de983a435aece603ff437ba7c69a5dff4ed0dc474f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8acc55d2f5754b808500b1d573b4a08455a08c2cd4ef0a6c611df7c5abf25d28 = $this->env->getExtension("native_profiler");
        $__internal_8acc55d2f5754b808500b1d573b4a08455a08c2cd4ef0a6c611df7c5abf25d28->enter($__internal_8acc55d2f5754b808500b1d573b4a08455a08c2cd4ef0a6c611df7c5abf25d28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("TwigBundle:Exception:exception.txt.twig", "TwigBundle:Exception:exception.js.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_8acc55d2f5754b808500b1d573b4a08455a08c2cd4ef0a6c611df7c5abf25d28->leave($__internal_8acc55d2f5754b808500b1d573b4a08455a08c2cd4ef0a6c611df7c5abf25d28_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include 'TwigBundle:Exception:exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
