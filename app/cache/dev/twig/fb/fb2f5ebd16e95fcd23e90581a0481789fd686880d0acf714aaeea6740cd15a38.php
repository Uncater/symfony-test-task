<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_986f0ec7b46963d64d08189933fe5dcd21f13b17cf41b422a271959ea7dd683a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_eca0e633af1cf180d9f18cbad9b63f36ec30b1fc39e7f4da3695a21c4c5a761d = $this->env->getExtension("native_profiler");
        $__internal_eca0e633af1cf180d9f18cbad9b63f36ec30b1fc39e7f4da3695a21c4c5a761d->enter($__internal_eca0e633af1cf180d9f18cbad9b63f36ec30b1fc39e7f4da3695a21c4c5a761d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_eca0e633af1cf180d9f18cbad9b63f36ec30b1fc39e7f4da3695a21c4c5a761d->leave($__internal_eca0e633af1cf180d9f18cbad9b63f36ec30b1fc39e7f4da3695a21c4c5a761d_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
