<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_0a345343ce55dd735825e0d3c1e107cd58d2f6640471ba521596462af5e82dea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a8489a9857e68fbc022fb6e8c31a2422887c7dcdb69169dab531ae8793695abb = $this->env->getExtension("native_profiler");
        $__internal_a8489a9857e68fbc022fb6e8c31a2422887c7dcdb69169dab531ae8793695abb->enter($__internal_a8489a9857e68fbc022fb6e8c31a2422887c7dcdb69169dab531ae8793695abb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_a8489a9857e68fbc022fb6e8c31a2422887c7dcdb69169dab531ae8793695abb->leave($__internal_a8489a9857e68fbc022fb6e8c31a2422887c7dcdb69169dab531ae8793695abb_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_c92a19802dcb4e1dc633205af46bb9a5b483e5288d1f4cf008e2a57e74b811a4 = $this->env->getExtension("native_profiler");
        $__internal_c92a19802dcb4e1dc633205af46bb9a5b483e5288d1f4cf008e2a57e74b811a4->enter($__internal_c92a19802dcb4e1dc633205af46bb9a5b483e5288d1f4cf008e2a57e74b811a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_c92a19802dcb4e1dc633205af46bb9a5b483e5288d1f4cf008e2a57e74b811a4->leave($__internal_c92a19802dcb4e1dc633205af46bb9a5b483e5288d1f4cf008e2a57e74b811a4_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }
}
/* {% block panel '' %}*/
/* */
