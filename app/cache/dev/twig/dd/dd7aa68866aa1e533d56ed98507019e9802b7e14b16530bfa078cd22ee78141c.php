<?php

/* FOSUserBundle:Resetting:request.html.twig */
class __TwigTemplate_5e61aef49e5e03a565b3623da5dbd1b9a50e70030be20ed3c81caf43f2d03746 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Resetting:request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e33835a5400b60e950c49a9efca819de80792d3f2fc2835d0d4acad4e9430078 = $this->env->getExtension("native_profiler");
        $__internal_e33835a5400b60e950c49a9efca819de80792d3f2fc2835d0d4acad4e9430078->enter($__internal_e33835a5400b60e950c49a9efca819de80792d3f2fc2835d0d4acad4e9430078_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e33835a5400b60e950c49a9efca819de80792d3f2fc2835d0d4acad4e9430078->leave($__internal_e33835a5400b60e950c49a9efca819de80792d3f2fc2835d0d4acad4e9430078_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_ad6d9383085dc4d8235e85941b41b3f6b4d34dbefe95d2ebc83bb15ef2776fa7 = $this->env->getExtension("native_profiler");
        $__internal_ad6d9383085dc4d8235e85941b41b3f6b4d34dbefe95d2ebc83bb15ef2776fa7->enter($__internal_ad6d9383085dc4d8235e85941b41b3f6b4d34dbefe95d2ebc83bb15ef2776fa7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Resetting:request_content.html.twig", "FOSUserBundle:Resetting:request.html.twig", 4)->display($context);
        
        $__internal_ad6d9383085dc4d8235e85941b41b3f6b4d34dbefe95d2ebc83bb15ef2776fa7->leave($__internal_ad6d9383085dc4d8235e85941b41b3f6b4d34dbefe95d2ebc83bb15ef2776fa7_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Resetting:request_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
