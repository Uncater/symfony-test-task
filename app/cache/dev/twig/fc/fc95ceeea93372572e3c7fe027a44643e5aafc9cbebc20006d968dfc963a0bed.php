<?php

/* FOSUserBundle:Resetting:passwordAlreadyRequested.html.twig */
class __TwigTemplate_38d5b521a50a36f40466994e3af4b10ec66333bd7d8e3d595b7fc1975e67b9a1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Resetting:passwordAlreadyRequested.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b272981250504bfb1342daf18065894e86f0e7c2dea3f1501f7a25150d71bb93 = $this->env->getExtension("native_profiler");
        $__internal_b272981250504bfb1342daf18065894e86f0e7c2dea3f1501f7a25150d71bb93->enter($__internal_b272981250504bfb1342daf18065894e86f0e7c2dea3f1501f7a25150d71bb93_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:passwordAlreadyRequested.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b272981250504bfb1342daf18065894e86f0e7c2dea3f1501f7a25150d71bb93->leave($__internal_b272981250504bfb1342daf18065894e86f0e7c2dea3f1501f7a25150d71bb93_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_623e65144e0447a1b4b5a809117daa875058b260b900e0dfc81ecbf39645084f = $this->env->getExtension("native_profiler");
        $__internal_623e65144e0447a1b4b5a809117daa875058b260b900e0dfc81ecbf39645084f->enter($__internal_623e65144e0447a1b4b5a809117daa875058b260b900e0dfc81ecbf39645084f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.password_already_requested", array(), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_623e65144e0447a1b4b5a809117daa875058b260b900e0dfc81ecbf39645084f->leave($__internal_623e65144e0447a1b4b5a809117daa875058b260b900e0dfc81ecbf39645084f_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:passwordAlreadyRequested.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/* <p>{{ 'resetting.password_already_requested'|trans }}</p>*/
/* {% endblock fos_user_content %}*/
/* */
