<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_bb7559e85ec0b24ee9d12ba4796d4508b979ef92c4e410ae106a02624b0d705a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8df7972a3ab9372660f670eec5c001f03694a4e78b8b360a33e703bc4b286eef = $this->env->getExtension("native_profiler");
        $__internal_8df7972a3ab9372660f670eec5c001f03694a4e78b8b360a33e703bc4b286eef->enter($__internal_8df7972a3ab9372660f670eec5c001f03694a4e78b8b360a33e703bc4b286eef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8df7972a3ab9372660f670eec5c001f03694a4e78b8b360a33e703bc4b286eef->leave($__internal_8df7972a3ab9372660f670eec5c001f03694a4e78b8b360a33e703bc4b286eef_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_a25f3899763d7c780efd8e3b129b7701970109acf08df41cdbb40afdb6fd0a3a = $this->env->getExtension("native_profiler");
        $__internal_a25f3899763d7c780efd8e3b129b7701970109acf08df41cdbb40afdb6fd0a3a->enter($__internal_a25f3899763d7c780efd8e3b129b7701970109acf08df41cdbb40afdb6fd0a3a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_a25f3899763d7c780efd8e3b129b7701970109acf08df41cdbb40afdb6fd0a3a->leave($__internal_a25f3899763d7c780efd8e3b129b7701970109acf08df41cdbb40afdb6fd0a3a_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_ef49e6e70f37ef0e4624a38b4dc53061d4b81993f1ac38df17ff38c0407c9ac3 = $this->env->getExtension("native_profiler");
        $__internal_ef49e6e70f37ef0e4624a38b4dc53061d4b81993f1ac38df17ff38c0407c9ac3->enter($__internal_ef49e6e70f37ef0e4624a38b4dc53061d4b81993f1ac38df17ff38c0407c9ac3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_ef49e6e70f37ef0e4624a38b4dc53061d4b81993f1ac38df17ff38c0407c9ac3->leave($__internal_ef49e6e70f37ef0e4624a38b4dc53061d4b81993f1ac38df17ff38c0407c9ac3_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_c89b9bb07cc23ab4505f69710596704b6429e63ca468c1f825dff469cd78e81f = $this->env->getExtension("native_profiler");
        $__internal_c89b9bb07cc23ab4505f69710596704b6429e63ca468c1f825dff469cd78e81f->enter($__internal_c89b9bb07cc23ab4505f69710596704b6429e63ca468c1f825dff469cd78e81f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_c89b9bb07cc23ab4505f69710596704b6429e63ca468c1f825dff469cd78e81f->leave($__internal_c89b9bb07cc23ab4505f69710596704b6429e63ca468c1f825dff469cd78e81f_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'TwigBundle::layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include 'TwigBundle:Exception:exception.html.twig' %}*/
/* {% endblock %}*/
/* */
