<?php

/* FOSUserBundle:Resetting:reset.html.twig */
class __TwigTemplate_e31f665c5169d59fecf4b9949a1b6e615bf676181b8a2fb8bb03da416fab942b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0d5a9f3901872fef13657f261afa1a4daac124454ad78d200eca204b1323f17f = $this->env->getExtension("native_profiler");
        $__internal_0d5a9f3901872fef13657f261afa1a4daac124454ad78d200eca204b1323f17f->enter($__internal_0d5a9f3901872fef13657f261afa1a4daac124454ad78d200eca204b1323f17f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0d5a9f3901872fef13657f261afa1a4daac124454ad78d200eca204b1323f17f->leave($__internal_0d5a9f3901872fef13657f261afa1a4daac124454ad78d200eca204b1323f17f_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_d6a81c8dd23d9c5a6061c56c755e13f629725ecc971579bafa4b6e60fa70f813 = $this->env->getExtension("native_profiler");
        $__internal_d6a81c8dd23d9c5a6061c56c755e13f629725ecc971579bafa4b6e60fa70f813->enter($__internal_d6a81c8dd23d9c5a6061c56c755e13f629725ecc971579bafa4b6e60fa70f813_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Resetting:reset_content.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 4)->display($context);
        
        $__internal_d6a81c8dd23d9c5a6061c56c755e13f629725ecc971579bafa4b6e60fa70f813->leave($__internal_d6a81c8dd23d9c5a6061c56c755e13f629725ecc971579bafa4b6e60fa70f813_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Resetting:reset_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
