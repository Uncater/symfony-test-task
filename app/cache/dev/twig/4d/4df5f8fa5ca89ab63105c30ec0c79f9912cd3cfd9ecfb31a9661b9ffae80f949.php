<?php

/* hello.html.twig */
class __TwigTemplate_d2fc611e53777e83c5738cad57410247f65c626e7c33214774ffd6a95ef270d8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "hello.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bbb3066b77ae0608e0951dc08a4fca738a51f5d7494b65aaf8f955dc72fb5ba2 = $this->env->getExtension("native_profiler");
        $__internal_bbb3066b77ae0608e0951dc08a4fca738a51f5d7494b65aaf8f955dc72fb5ba2->enter($__internal_bbb3066b77ae0608e0951dc08a4fca738a51f5d7494b65aaf8f955dc72fb5ba2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "hello.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bbb3066b77ae0608e0951dc08a4fca738a51f5d7494b65aaf8f955dc72fb5ba2->leave($__internal_bbb3066b77ae0608e0951dc08a4fca738a51f5d7494b65aaf8f955dc72fb5ba2_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_32f9760db0565c5e9e91ed4cb36044bb4b7525f2c54349be4e8d8b12a34d6ecf = $this->env->getExtension("native_profiler");
        $__internal_32f9760db0565c5e9e91ed4cb36044bb4b7525f2c54349be4e8d8b12a34d6ecf->enter($__internal_32f9760db0565c5e9e91ed4cb36044bb4b7525f2c54349be4e8d8b12a34d6ecf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Referral links application";
        
        $__internal_32f9760db0565c5e9e91ed4cb36044bb4b7525f2c54349be4e8d8b12a34d6ecf->leave($__internal_32f9760db0565c5e9e91ed4cb36044bb4b7525f2c54349be4e8d8b12a34d6ecf_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_f444be5d65d9217b4a5b6bf606a8ed17e48c5c5341a683df04b9ceda59d2c8d2 = $this->env->getExtension("native_profiler");
        $__internal_f444be5d65d9217b4a5b6bf606a8ed17e48c5c5341a683df04b9ceda59d2c8d2->enter($__internal_f444be5d65d9217b4a5b6bf606a8ed17e48c5c5341a683df04b9ceda59d2c8d2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div id=\"wrapper\">
        <div id=\"container\">
            <div id=\"welcome\">
                <h1>Hello there!</h1>
                <h2>Welcome to referral link application!</h2>
                <h3>If you want to create your own referral link, please register.
                </h3>
                ";
        // line 13
        $this->displayBlock('sidebar', $context, $blocks);
        // line 21
        echo "            </div>
        </div>
    </div>

";
        
        $__internal_f444be5d65d9217b4a5b6bf606a8ed17e48c5c5341a683df04b9ceda59d2c8d2->leave($__internal_f444be5d65d9217b4a5b6bf606a8ed17e48c5c5341a683df04b9ceda59d2c8d2_prof);

    }

    // line 13
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_3ea5e2b0ea207544b8c2189cf295b4770850f507898286860b36e57c7f07681f = $this->env->getExtension("native_profiler");
        $__internal_3ea5e2b0ea207544b8c2189cf295b4770850f507898286860b36e57c7f07681f->enter($__internal_3ea5e2b0ea207544b8c2189cf295b4770850f507898286860b36e57c7f07681f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 14
        echo "                    <ul>
                        <li><a href=\"/login\">Login</a></li>
                        <li><a href=\"/register/\">Register</a></li>
                        <li><a href=\"/show\">Show amount of refers</a> </li>
                        <li><a href=\"/create\">Create your referral link</a> </li>
                    </ul>
                ";
        
        $__internal_3ea5e2b0ea207544b8c2189cf295b4770850f507898286860b36e57c7f07681f->leave($__internal_3ea5e2b0ea207544b8c2189cf295b4770850f507898286860b36e57c7f07681f_prof);

    }

    public function getTemplateName()
    {
        return "hello.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 14,  76 => 13,  65 => 21,  63 => 13,  54 => 6,  48 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block title %}Referral links application{% endblock %}*/
/* */
/* {% block body %}*/
/*     <div id="wrapper">*/
/*         <div id="container">*/
/*             <div id="welcome">*/
/*                 <h1>Hello there!</h1>*/
/*                 <h2>Welcome to referral link application!</h2>*/
/*                 <h3>If you want to create your own referral link, please register.*/
/*                 </h3>*/
/*                 {% block sidebar %}*/
/*                     <ul>*/
/*                         <li><a href="/login">Login</a></li>*/
/*                         <li><a href="/register/">Register</a></li>*/
/*                         <li><a href="/show">Show amount of refers</a> </li>*/
/*                         <li><a href="/create">Create your referral link</a> </li>*/
/*                     </ul>*/
/*                 {% endblock %}*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/* {% endblock %}*/
