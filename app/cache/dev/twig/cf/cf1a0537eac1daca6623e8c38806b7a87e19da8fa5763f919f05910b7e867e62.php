<?php

/* FOSUserBundle:Group:show_content.html.twig */
class __TwigTemplate_ab1a326b3c9bbaa66e7d40d482ddac830a7e12334f31a16fe8d1f927a0f4ed90 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bdcd9790cec4fcc2e67932890dc99e274ea9d9624708ba778617424e6875fb69 = $this->env->getExtension("native_profiler");
        $__internal_bdcd9790cec4fcc2e67932890dc99e274ea9d9624708ba778617424e6875fb69->enter($__internal_bdcd9790cec4fcc2e67932890dc99e274ea9d9624708ba778617424e6875fb69_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_group_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("group.show.name", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["group"]) ? $context["group"] : $this->getContext($context, "group")), "getName", array(), "method"), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_bdcd9790cec4fcc2e67932890dc99e274ea9d9624708ba778617424e6875fb69->leave($__internal_bdcd9790cec4fcc2e67932890dc99e274ea9d9624708ba778617424e6875fb69_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 4,  22 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* <div class="fos_user_group_show">*/
/*     <p>{{ 'group.show.name'|trans }}: {{ group.getName() }}</p>*/
/* </div>*/
/* */
