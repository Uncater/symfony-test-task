<?php

/* TwigBundle:Exception:error.xml.twig */
class __TwigTemplate_e472e2176ebacec90f4a9f5fc9786440471a427a9e9a5c87977764a9fd416128 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cce1f63057d66f23c7ac5bafb96846cf20e3d8d901cf6a02b605a9ad2d29b105 = $this->env->getExtension("native_profiler");
        $__internal_cce1f63057d66f23c7ac5bafb96846cf20e3d8d901cf6a02b605a9ad2d29b105->enter($__internal_cce1f63057d66f23c7ac5bafb96846cf20e3d8d901cf6a02b605a9ad2d29b105_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo "\" message=\"";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo "\" />
";
        
        $__internal_cce1f63057d66f23c7ac5bafb96846cf20e3d8d901cf6a02b605a9ad2d29b105->leave($__internal_cce1f63057d66f23c7ac5bafb96846cf20e3d8d901cf6a02b605a9ad2d29b105_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  22 => 1,);
    }
}
/* <?xml version="1.0" encoding="{{ _charset }}" ?>*/
/* */
/* <error code="{{ status_code }}" message="{{ status_text }}" />*/
/* */
