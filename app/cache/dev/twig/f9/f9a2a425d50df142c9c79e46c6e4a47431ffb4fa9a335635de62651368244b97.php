<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_aab91e2eba2b1619f2204a59bbe30ac2a4ad7381d7ab79ccba62f66093eca4a0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0df8f2c33ff35f82ec440c01859fe1bca0d3969e6904bff01961145d7daeaba1 = $this->env->getExtension("native_profiler");
        $__internal_0df8f2c33ff35f82ec440c01859fe1bca0d3969e6904bff01961145d7daeaba1->enter($__internal_0df8f2c33ff35f82ec440c01859fe1bca0d3969e6904bff01961145d7daeaba1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        $this->loadTemplate("TwigBundle:Exception:exception.xml.twig", "TwigBundle:Exception:exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_0df8f2c33ff35f82ec440c01859fe1bca0d3969e6904bff01961145d7daeaba1->leave($__internal_0df8f2c33ff35f82ec440c01859fe1bca0d3969e6904bff01961145d7daeaba1_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include 'TwigBundle:Exception:exception.xml.twig' with { 'exception': exception } %}*/
/* */
