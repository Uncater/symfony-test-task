<?php

/* SensioDistributionBundle::Configurator/layout.html.twig */
class __TwigTemplate_84bb534472f17289ffb49b2ed3c635e78e8f1b6f40c3043cb7146e4a3f6eb317 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "SensioDistributionBundle::Configurator/layout.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_30916f5b718e137e229c3c2c6cfac9f207d46a4872052fbd8573afa8c2c94dc0 = $this->env->getExtension("native_profiler");
        $__internal_30916f5b718e137e229c3c2c6cfac9f207d46a4872052fbd8573afa8c2c94dc0->enter($__internal_30916f5b718e137e229c3c2c6cfac9f207d46a4872052fbd8573afa8c2c94dc0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SensioDistributionBundle::Configurator/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_30916f5b718e137e229c3c2c6cfac9f207d46a4872052fbd8573afa8c2c94dc0->leave($__internal_30916f5b718e137e229c3c2c6cfac9f207d46a4872052fbd8573afa8c2c94dc0_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_a50b3dccf1bf3ac35af03fbafc759e17636b5c2305b1b9a626fb9fb8108a485f = $this->env->getExtension("native_profiler");
        $__internal_a50b3dccf1bf3ac35af03fbafc759e17636b5c2305b1b9a626fb9fb8108a485f->enter($__internal_a50b3dccf1bf3ac35af03fbafc759e17636b5c2305b1b9a626fb9fb8108a485f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/sensiodistribution/webconfigurator/css/configurator.css"), "html", null, true);
        echo "\" />
";
        
        $__internal_a50b3dccf1bf3ac35af03fbafc759e17636b5c2305b1b9a626fb9fb8108a485f->leave($__internal_a50b3dccf1bf3ac35af03fbafc759e17636b5c2305b1b9a626fb9fb8108a485f_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_6285b8573980c87c63666bae70ac1959ef157a4c90a320af06e9ee1b85b7d62d = $this->env->getExtension("native_profiler");
        $__internal_6285b8573980c87c63666bae70ac1959ef157a4c90a320af06e9ee1b85b7d62d->enter($__internal_6285b8573980c87c63666bae70ac1959ef157a4c90a320af06e9ee1b85b7d62d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Web Configurator Bundle";
        
        $__internal_6285b8573980c87c63666bae70ac1959ef157a4c90a320af06e9ee1b85b7d62d->leave($__internal_6285b8573980c87c63666bae70ac1959ef157a4c90a320af06e9ee1b85b7d62d_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_7808269a1ee4231162a4725f48c8a5a131f969d6e485ca1a7099082c7bdfca03 = $this->env->getExtension("native_profiler");
        $__internal_7808269a1ee4231162a4725f48c8a5a131f969d6e485ca1a7099082c7bdfca03->enter($__internal_7808269a1ee4231162a4725f48c8a5a131f969d6e485ca1a7099082c7bdfca03_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "    <div class=\"block\">
        ";
        // line 11
        $this->displayBlock('content', $context, $blocks);
        // line 12
        echo "    </div>
    <div class=\"version\">Symfony Standard Edition v.";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["version"]) ? $context["version"] : $this->getContext($context, "version")), "html", null, true);
        echo "</div>
";
        
        $__internal_7808269a1ee4231162a4725f48c8a5a131f969d6e485ca1a7099082c7bdfca03->leave($__internal_7808269a1ee4231162a4725f48c8a5a131f969d6e485ca1a7099082c7bdfca03_prof);

    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        $__internal_c124a8af52bc85fab6b86ebade9e8a7154858759c2409033ee399cccdd4ca661 = $this->env->getExtension("native_profiler");
        $__internal_c124a8af52bc85fab6b86ebade9e8a7154858759c2409033ee399cccdd4ca661->enter($__internal_c124a8af52bc85fab6b86ebade9e8a7154858759c2409033ee399cccdd4ca661_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_c124a8af52bc85fab6b86ebade9e8a7154858759c2409033ee399cccdd4ca661->leave($__internal_c124a8af52bc85fab6b86ebade9e8a7154858759c2409033ee399cccdd4ca661_prof);

    }

    public function getTemplateName()
    {
        return "SensioDistributionBundle::Configurator/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 11,  79 => 13,  76 => 12,  74 => 11,  71 => 10,  65 => 9,  53 => 7,  43 => 4,  37 => 3,  11 => 1,);
    }
}
/* {% extends "TwigBundle::layout.html.twig" %}*/
/* */
/* {% block head %}*/
/*     <link rel="stylesheet" href="{{ asset('bundles/sensiodistribution/webconfigurator/css/configurator.css') }}" />*/
/* {% endblock %}*/
/* */
/* {% block title 'Web Configurator Bundle' %}*/
/* */
/* {% block body %}*/
/*     <div class="block">*/
/*         {% block content %}{% endblock %}*/
/*     </div>*/
/*     <div class="version">Symfony Standard Edition v.{{ version }}</div>*/
/* {% endblock %}*/
/* */
