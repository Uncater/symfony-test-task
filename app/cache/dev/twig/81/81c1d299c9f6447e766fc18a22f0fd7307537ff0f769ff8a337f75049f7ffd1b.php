<?php

/* FOSUserBundle:ChangePassword:changePassword.html.twig */
class __TwigTemplate_135b2da7e11c171aefcad21f6cf27688389c287542479f09809eab30a8662dea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:ChangePassword:changePassword.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a7848ce36cd8eeea551f7da936a1c81828f6177b8f42b41d304b8161cc906669 = $this->env->getExtension("native_profiler");
        $__internal_a7848ce36cd8eeea551f7da936a1c81828f6177b8f42b41d304b8161cc906669->enter($__internal_a7848ce36cd8eeea551f7da936a1c81828f6177b8f42b41d304b8161cc906669_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:changePassword.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a7848ce36cd8eeea551f7da936a1c81828f6177b8f42b41d304b8161cc906669->leave($__internal_a7848ce36cd8eeea551f7da936a1c81828f6177b8f42b41d304b8161cc906669_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_71414f12981598e87fdc100a926145dadebf3ea5f68c9609c555f14dbafd7677 = $this->env->getExtension("native_profiler");
        $__internal_71414f12981598e87fdc100a926145dadebf3ea5f68c9609c555f14dbafd7677->enter($__internal_71414f12981598e87fdc100a926145dadebf3ea5f68c9609c555f14dbafd7677_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:ChangePassword:changePassword_content.html.twig", "FOSUserBundle:ChangePassword:changePassword.html.twig", 4)->display($context);
        
        $__internal_71414f12981598e87fdc100a926145dadebf3ea5f68c9609c555f14dbafd7677->leave($__internal_71414f12981598e87fdc100a926145dadebf3ea5f68c9609c555f14dbafd7677_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:changePassword.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:ChangePassword:changePassword_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
