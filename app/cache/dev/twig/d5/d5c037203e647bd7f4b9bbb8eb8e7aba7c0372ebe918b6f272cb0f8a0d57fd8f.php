<?php

/* FOSUserBundle:Group:list.html.twig */
class __TwigTemplate_a369dbdb24fbfb1e86d557cbb780b55ef1efed2ed473d4727818e9be74091b57 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Group:list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_60bd89f3854bf8f119d82aa43e2e784806c2962d8cc1cf81cf4362ed4ae23750 = $this->env->getExtension("native_profiler");
        $__internal_60bd89f3854bf8f119d82aa43e2e784806c2962d8cc1cf81cf4362ed4ae23750->enter($__internal_60bd89f3854bf8f119d82aa43e2e784806c2962d8cc1cf81cf4362ed4ae23750_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_60bd89f3854bf8f119d82aa43e2e784806c2962d8cc1cf81cf4362ed4ae23750->leave($__internal_60bd89f3854bf8f119d82aa43e2e784806c2962d8cc1cf81cf4362ed4ae23750_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_87e4d96758787b0327746210de1787fea70d4eda9bbbcc8dc2ac850d464313e1 = $this->env->getExtension("native_profiler");
        $__internal_87e4d96758787b0327746210de1787fea70d4eda9bbbcc8dc2ac850d464313e1->enter($__internal_87e4d96758787b0327746210de1787fea70d4eda9bbbcc8dc2ac850d464313e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:list_content.html.twig", "FOSUserBundle:Group:list.html.twig", 4)->display($context);
        
        $__internal_87e4d96758787b0327746210de1787fea70d4eda9bbbcc8dc2ac850d464313e1->leave($__internal_87e4d96758787b0327746210de1787fea70d4eda9bbbcc8dc2ac850d464313e1_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:list_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
