<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_af9b4e296a2dabf3d1e0c5091c181330eb799ce932926b2102767aec1c31c800 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dbcfa59c66949875c097a0fa1df5f4e38db52c065c5832b57586384cf19cfe39 = $this->env->getExtension("native_profiler");
        $__internal_dbcfa59c66949875c097a0fa1df5f4e38db52c065c5832b57586384cf19cfe39->enter($__internal_dbcfa59c66949875c097a0fa1df5f4e38db52c065c5832b57586384cf19cfe39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_dbcfa59c66949875c097a0fa1df5f4e38db52c065c5832b57586384cf19cfe39->leave($__internal_dbcfa59c66949875c097a0fa1df5f4e38db52c065c5832b57586384cf19cfe39_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_71045fc4f2ed31b7aba824100d5a084c525372c35d88b87f8e180d7ab4723f4d = $this->env->getExtension("native_profiler");
        $__internal_71045fc4f2ed31b7aba824100d5a084c525372c35d88b87f8e180d7ab4723f4d->enter($__internal_71045fc4f2ed31b7aba824100d5a084c525372c35d88b87f8e180d7ab4723f4d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_71045fc4f2ed31b7aba824100d5a084c525372c35d88b87f8e180d7ab4723f4d->leave($__internal_71045fc4f2ed31b7aba824100d5a084c525372c35d88b87f8e180d7ab4723f4d_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_043966b23bb9f535bae1c8fe3c4aeee8ca8b4dfec0299b6d1deb930a1049bc0c = $this->env->getExtension("native_profiler");
        $__internal_043966b23bb9f535bae1c8fe3c4aeee8ca8b4dfec0299b6d1deb930a1049bc0c->enter($__internal_043966b23bb9f535bae1c8fe3c4aeee8ca8b4dfec0299b6d1deb930a1049bc0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_043966b23bb9f535bae1c8fe3c4aeee8ca8b4dfec0299b6d1deb930a1049bc0c->leave($__internal_043966b23bb9f535bae1c8fe3c4aeee8ca8b4dfec0299b6d1deb930a1049bc0c_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends 'TwigBundle::layout.html.twig' %}*/
/* */
/* {% block title 'Redirection Intercepted' %}*/
/* */
/* {% block body %}*/
/*     <div class="sf-reset">*/
/*         <div class="block-exception">*/
/*             <h1>This request redirects to <a href="{{ location }}">{{ location }}</a>.</h1>*/
/* */
/*             <p>*/
/*                 <small>*/
/*                     The redirect was intercepted by the web debug toolbar to help debugging.*/
/*                     For more information, see the "intercept-redirects" option of the Profiler.*/
/*                 </small>*/
/*             </p>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
