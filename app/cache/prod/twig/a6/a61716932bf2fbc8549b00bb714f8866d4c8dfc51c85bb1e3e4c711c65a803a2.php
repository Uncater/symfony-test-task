<?php

/* ::hello.html.twig */
class __TwigTemplate_9701ec43c1c64a928deed3b7be291d1174fa3708c35014d0c95879d7ea2d466e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "::hello.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Referral links application";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "    <div id=\"wrapper\">
        <div id=\"container\">
            <div id=\"welcome\">
                <h1>Hello there!</h1>
                <h2>Welcome to referral link application!</h2>
                <h3>If you want to create your own referral link, please register.
                </h3>
                ";
        // line 13
        $this->displayBlock('sidebar', $context, $blocks);
        // line 21
        echo "            </div>
        </div>
    </div>

";
    }

    // line 13
    public function block_sidebar($context, array $blocks = array())
    {
        // line 14
        echo "                    <ul>
                        <li><a href=\"/login\">Login</a></li>
                        <li><a href=\"/register/\">Register</a></li>
                        <li><a href=\"/show\">Show amount of refers</a> </li>
                        <li><a href=\"/create\">Create your referral link</a> </li>
                    </ul>
                ";
    }

    public function getTemplateName()
    {
        return "::hello.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 14,  58 => 13,  50 => 21,  48 => 13,  39 => 6,  36 => 5,  30 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block title %}Referral links application{% endblock %}*/
/* */
/* {% block body %}*/
/*     <div id="wrapper">*/
/*         <div id="container">*/
/*             <div id="welcome">*/
/*                 <h1>Hello there!</h1>*/
/*                 <h2>Welcome to referral link application!</h2>*/
/*                 <h3>If you want to create your own referral link, please register.*/
/*                 </h3>*/
/*                 {% block sidebar %}*/
/*                     <ul>*/
/*                         <li><a href="/login">Login</a></li>*/
/*                         <li><a href="/register/">Register</a></li>*/
/*                         <li><a href="/show">Show amount of refers</a> </li>*/
/*                         <li><a href="/create">Create your referral link</a> </li>*/
/*                     </ul>*/
/*                 {% endblock %}*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/* {% endblock %}*/
