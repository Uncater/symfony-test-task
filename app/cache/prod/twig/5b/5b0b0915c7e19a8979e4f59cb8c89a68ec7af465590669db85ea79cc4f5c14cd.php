<?php

/* ::form.html.twig */
class __TwigTemplate_777589dca1b56338cce4571f850b6332a9eabdd29de2c1709c74bfab13e02a96 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"center\">
    ";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : null), "html", null, true);
        echo "<br>
    <button type=\"button\" class=\"\"><a href=\"/app_dev.php\">Go to homepage</a></button></button>
    </div>
<style>
.center {
    margin: auto;
    width: 45%;
    border: 3px solid #73AD21;
    padding: 15px;
}

</style>";
    }

    public function getTemplateName()
    {
        return "::form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 2,  19 => 1,);
    }
}
/* <div class="center">*/
/*     {{ message}}<br>*/
/*     <button type="button" class=""><a href="/app_dev.php">Go to homepage</a></button></button>*/
/*     </div>*/
/* <style>*/
/* .center {*/
/*     margin: auto;*/
/*     width: 45%;*/
/*     border: 3px solid #73AD21;*/
/*     padding: 15px;*/
/* }*/
/* */
/* </style>*/
