<?php

/* FOSUserBundle::layout.html.twig */
class __TwigTemplate_06a11129a6b996a4ad2552746671a1ecb125ecc160894387aae5d2b3362163f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "FOSUserBundle::layout.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Referral links applacation";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "    <div id=\"wrapper\">
    <div id=\"container\">
    <div id=\"welcome\">
        <h1><span>Welcome!</h1>
    </div>
    <div class=\"block\">
    ";
        // line 12
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 13
        echo "</div>
    </div>
    </div>

";
    }

    // line 12
    public function block_fos_user_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "FOSUserBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 12,  49 => 13,  47 => 12,  39 => 6,  36 => 5,  30 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block title %}Referral links applacation{% endblock %}*/
/* */
/* {% block body %}*/
/*     <div id="wrapper">*/
/*     <div id="container">*/
/*     <div id="welcome">*/
/*         <h1><span>Welcome!</h1>*/
/*     </div>*/
/*     <div class="block">*/
/*     {% block fos_user_content %}{% endblock %}*/
/* </div>*/
/*     </div>*/
/*     </div>*/
/* */
/* {% endblock %}*/
