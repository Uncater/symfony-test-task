<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Referal;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class DefaultController extends Controller
{



    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('hello.html.twig');
    }



    /**
     * @Route("/referal", name = "referal")
     */

//    Updating existing one
    public function updateReferal(Request $request)
    {

        $slug = $request->query->get('ref');
        $refLink = $this->generateUrl('referal', array('ref' => $slug), UrlGeneratorInterface::ABSOLUTE_URL);
        $em = $this->getDoctrine()->getManager();
        $referal = $em->getRepository('AppBundle:Referal')->findOneByrefLink($refLink);
        if (!$referal) {
            throw $this->createNotFoundException(
                'No such referal link '.$refLink
            );
        }
        $num = $referal->getRefNum();
        $referal->setRefNum($num+1);
        $em->flush();
        $message = 'You have just given your friend +1 to karma! We are sure he is grateful for it!';

        return $this->render('form.html.twig', array('message' => $message));


    }

    /**
     * @Route("/create", name = "create")
     */
//New referal link
    public function newRef()
    {
        do{
        $refLink = substr(uniqid(),0,6);
        $em = $this->getDoctrine()->getManager();
        $ref = $em->getRepository('AppBundle:Referal')->findOneByrefLink($refLink);}

        while($ref);

            $url = $this->generateUrl('referal', array('ref' => $refLink), UrlGeneratorInterface::ABSOLUTE_URL);
            $em = $this->getDoctrine()->getManager();

            $user = $this->container->get('security.context')->getToken()->getUser();
            $link = $em->getRepository('AppBundle:Referal')->findOneByUser($user);
            $temp = $link->getRefLink();
//        var_dump($temp);
            if($em->getRepository('AppBundle:Referal')->findOneByUser($user))
            {

                $message = 'Here is your link:  ' . $temp ;
                return $this->render('form.html.twig', array('message' => $message));
            }
            $referal = new Referal();
            $referal->setRefLink($url);
            $referal->setRefNum(0);
            $referal->setUser($user);
            $em->persist($referal);
            $em->flush();
            $message = 'Your referal link is:' . $url;


        return $this->render('form.html.twig', array('message' => $message));

        }

    /**
     * @Route("/show", name = "show")
     */
    public function showRef()
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $link = $em->getRepository('AppBundle:Referal')->findOneByUser($user);
        $refNum = $link->getRefNum();

        $message = 'At the current moment your link have ' .$refNum .' refers.';

        return $this->render('form.html.twig', array('message' => $message));

    }



}



