<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="refers")
 */

class Referal
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\OneToOne(targetEntity = "AppBundle\Entity\User", inversedBy = "referal")
     * @ORM\JoinColumn(name = "user_id", referencedColumnName = "id")
     */
    protected $user;



    /**
     * @ORM\Column(type="integer")
     */
    protected $refNum;

    /**
     * @ORM\Column(type="text")
     */
    protected $refLink;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set refNum
     *
     * @param integer $refNum
     *
     * @return Referal
     */
    public function setRefNum($refNum)
    {
        $this->refNum = $refNum;

        return $this;
    }

    /**
     * Get refNum
     *
     * @return integer
     */
    public function getRefNum()
    {
        return $this->refNum;
    }


    /**
     * Set refLink
     *
     * @param string $refLink
     *
     * @return Referal
     */
    public function setRefLink($refLink)
    {
        $this->refLink = $refLink;

        return $this;
    }

    /**
     * Get refLink
     *
     * @return string
     */
    public function getRefLink()
    {
        return $this->refLink;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Referal
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
