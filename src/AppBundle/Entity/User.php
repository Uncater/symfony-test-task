<?php

// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
* @ORM\Table(name="fos_user")
*/
class User extends BaseUser
{
/**
* @ORM\Id
* @ORM\Column(type="integer")
* @ORM\GeneratedValue(strategy="AUTO")
*/
protected $id;

    /**
     *@ORM\OneToOne(targetEntity="AppBundle\Entity\Referal", mappedBy="user")
     */

protected $referal;


public function __construct()
{
parent::__construct();
// your own logic
}

    /**
     * Set referal
     *
     * @param \AppBundle\Entity\Referal $referal
     *
     * @return User
     */
    public function setReferal(\AppBundle\Entity\Referal $referal = null)
    {
        $this->referal = $referal;

        return $this;
    }

    /**
     * Get referal
     *
     * @return \AppBundle\Entity\Referal
     */
    public function getReferal()
    {
        return $this->referal;
    }
}
